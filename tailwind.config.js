/** @type {import('tailwindcss').Config} */
import colors from 'tailwindcss/colors'
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    fontFamily: {
      default: ['Noto Sans', 'sans-serif']
    },
    colors: {
      // import default color
      ...colors,
      red: '#FC165B',
      green: '#16FCD2',
      'card-dark': '#161629',
      primary: '#6016FC',
      muted: '#FFFFFF60',
      highlight: '#221048',
      light: '#FFFFFF05',
      'button-light': '#FFFFFF10',
      'primary-light': '#6016FC10',
      dark: '#0B0B22,'
    },
    extend: {},
  },
  plugins: [],
}

